import ConfigParser

from flask import Flask

app = Flask(__name__)

    
def init(app):
    config = ConfigParser.ConfigParser()
    try:
        config_location = "etc/defaults.cfg"
        config.read(config_location)
        
        app.config['DEBUG'] = config.get("config", "debug")
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port")
        app.config['url'] = config.get("config", "url")
    except:
        print "Cannot access config file"

@app.route("/")
def index():
    return "Hali"

        
if __name__ == "__main__":
    init(app)
    app.run(
        host = app.config['ip_address'],
        port = int(app.config['port']))    
        
        
